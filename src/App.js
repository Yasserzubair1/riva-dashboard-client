// Styles
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';

import React, { Component } from "react";
import store, { history } from './configureStore';

import { ConnectedRouter } from 'connected-react-router'
import Footer from './containers/Footer'
// header and footer
import Header from './containers/Header'
import { Provider } from 'react-redux'
import SideBar from './containers/SideBar'
// routes
import routes from './routes';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div className="App">
            <Header />
            <div className="wrap">
                <div className='col-sm-12'>
              <div className='row'>
                  <div className='col-sm-3'>
                    <div className='panel'>
                      <SideBar/>
                    </div>
                  </div>
                  <div className='col-sm-8'>
                      {routes}
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
